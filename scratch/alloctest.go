package main

import (
	"log"
	"time"

	"github.com/spaolacci/murmur3"
	"gitlab.com/clbr/hummingbird/pkg/hbutil"
)

func main() {
	for j := 1; j < 31; j++ {
		startTime := time.Now()
		bitsize := uint32(j)
		magnitude := uint32(1 << bitsize)
		var testArray []uint32
		testArray = make([]uint32, 1<<bitsize)
		for i := uint32(1); i < magnitude; i++ {
			testArray[i] = murmur3.Sum32(hbutil.Uint32ToBytes(testArray[i-1]))
		}
		elapsedTime := time.Since(startTime)
		log.Println("Time to generate", magnitude, "hashes", elapsedTime, "Memory used for HashChain", len(testArray)*4/1024/1024, "Mb")
	}
}

/* benchmark result on i5-7600 CPU with DDR4-2400 memory:

loki@monolith:~/go/src/gitlab.com/clbr/hummingbird$ go run scratch/alloctest.go
2018/03/01 09:14:59 Time to generate 2 hashes 286.949µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 4 hashes 676ns Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 8 hashes 673ns Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 16 hashes 1.329µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 32 hashes 2.458µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 64 hashes 10.391µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 128 hashes 25.365µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 256 hashes 37.931µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 512 hashes 83.452µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 1024 hashes 219.58µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 2048 hashes 515.91µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 4096 hashes 878.388µs Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 8192 hashes 1.265167ms Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 16384 hashes 2.398871ms Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 32768 hashes 4.655194ms Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 65536 hashes 8.304318ms Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 131072 hashes 15.45315ms Memory used for HashChain 0 Mb
2018/03/01 09:14:59 Time to generate 262144 hashes 28.159321ms Memory used for HashChain 1 Mb
2018/03/01 09:14:59 Time to generate 524288 hashes 52.313037ms Memory used for HashChain 2 Mb
2018/03/01 09:14:59 Time to generate 1048576 hashes 100.487096ms Memory used for HashChain 4 Mb
2018/03/01 09:14:59 Time to generate 2097152 hashes 193.147865ms Memory used for HashChain 8 Mb
2018/03/01 09:14:59 Time to generate 4194304 hashes 386.367503ms Memory used for HashChain 16 Mb
2018/03/01 09:15:00 Time to generate 8388608 hashes 778.825413ms Memory used for HashChain 32 Mb
2018/03/01 09:15:02 Time to generate 16777216 hashes 1.543708116s Memory used for HashChain 64 Mb
2018/03/01 09:15:05 Time to generate 33554432 hashes 3.111111506s Memory used for HashChain 128 Mb
2018/03/01 09:15:11 Time to generate 67108864 hashes 6.180384348s Memory used for HashChain 256 Mb
2018/03/01 09:15:23 Time to generate 134217728 hashes 12.391666116s Memory used for HashChain 512 Mb
2018/03/01 09:15:48 Time to generate 268435456 hashes 24.727528079s Memory used for HashChain 1024 Mb
2018/03/01 09:16:38 Time to generate 536870912 hashes 49.644975168s Memory used for HashChain 2048 Mb
2018/03/01 09:18:17 Time to generate 1073741824 hashes 1m39.616445666s Memory used for HashChain 4096 Mb
*/
