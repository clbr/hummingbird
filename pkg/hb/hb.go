// Package hbutil - utility functions needed for Hummingbird

package hb

import (
	"crypto/rand"
	"encoding/binary"
	"log"
)

// GetRandUint32 - gets a cryptographically secure pseudorandom byte slice and converts it to uint32
func GetRandUint32() uint32 {
	r := make([]byte, 4)
	n, err := rand.Read(r)
	if n != 4 {
		log.Fatal("For some reason unable to get 4 random bytes")
	}
	if err != nil {
		log.Fatal(err)
	}
	return BytesToUint32(r)
}

// Uint32ToBytes - returns a byte slice from uint32 - required because Murmur3 takes bytes as input but returns uint32
func Uint32ToBytes(input uint32) []byte {
	p := make([]byte, 4)
	binary.LittleEndian.PutUint32(p, input)
	return p
}

// BytesToUint32 - converts 4 byte slice to uint32
func BytesToUint32(bytes []byte) uint32 {
	if len(bytes) != 4 {
		log.Fatal("Byte slice is not 4 bytes long")
	}
	return binary.LittleEndian.Uint32(bytes)
}

// Head - takes a uint32 and returns the first 16 bits
func Head(input uint32) uint16 {
	return uint16((input << 16) >> 16)
}

// Tail - takes a uint32 and returns the second 16 bits
func Tail(input uint32) uint16 {
	return uint16(input >> 16)
}
