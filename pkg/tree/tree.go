// Package tree - data types and functionality for Hummingbird solution searching
package tree

// Tree - Stores nodes that link to form search trees. Each node has binary tree references to enable head and tail searching.
type Tree struct {
	// Parent is the back-reference to a node that points left or right to this.
	Parent,
	// Left is a node with data that is greater than the data of this node.
	More,
	// Right is a node with data that is less than the data of this node.
	Less [2]uint32
	// Child is used only by the other search phases, and only one is needed for making linked lists and candidate cycles
	Child uint32
	// Data is the index of the position in the HashChain this node references.
	Data uint32
	// MaxGT - records the length of the longest path left (More)
	MaxGT,
	// MaxLT - records the length of the longest path right (Less)
	MaxLT uint32
}

// Rhizome - Stores a tree and its state information
type Rhizome struct {
	// BTree - An array containing the nodes in a binary search tree
	BTree []Tree
	// Root - Designates the node which is the root of the tree. This changes when a scapegoat re-sort occurs to optimise path lengths
	Root uint32
	// End - the position of the last node in the chain, updated when a new node is added. More convenient than len(Rhizome)
	End uint32
}

// New - creates a new Searches array to store searches for hashchain cycles
func New() Rhizome {
	var tree Rhizome
	return tree
}

// Insert - add a new search item to tree
func Insert(data uint32) {

}
