// Package hbtree - Implements a Scapegoat Binary Search Tree for Hummingbird's search strategy

package root

import (
	"gitlab.com/clbr/hummingbird/pkg/chain"
	"gitlab.com/clbr/hummingbird/pkg/tree"
)

// Root - joins head and tail Search trees - stores the min and max head/tail hash value and pointers to the root node of head and tail search trees
type Root struct {
	// Chain - the hashchain that is searched
	Chain chain.Chain
	// Target - the number of nodes in a cycle targeted by the search.
	Target uint16
	// Searches - tree containing nodes not yet found to branch, collide or chain
	Searches []tree.Tree
}

// Result - data structure containing a solution found by the solver
type Result struct {
	// Nonce - the cryptographic RNG generated seed
	Nonce uint32
	// Terminal - the index of the end-most vector in the hash chain in this result
	Terminal uint32
	// Cycle - a string from the hashchain of Nonce that forms a loop without branching from the vector at the end of its chain
	Cycle []uint32
}

// New - creates a new search root structure for organising the search for hashchain cycles
func New(maxIndices uint32) Root {
	var root Root
	root.Chain = chain.New(maxIndices)
	return root
}

// Search - Start the search process
func Search() Result {
	var result Result
	return result
}
