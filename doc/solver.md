# Hummingbird Proof of Work Solver Specification

This is a brief, general description of how the solver works.

## Data Structures

### 1. Searches

Each Searches node has a pair of parent, more and less references, a total of 6.

The root node connects to a special empty node that signifies the child of it is the root node. There usually will be two roots for Heads and Tails, both will refer to the Search Root node.

### 2. Branches

In order to improve memory allocation performance, nodes are never deleted.

Branches thus use the same structure, and add a Child reference, because branches mean collisions on the Head or Tail and there is only one link between two chained nodes, from the tail to the next head.

Furthermore, any head/head or tail/tail collision implicitly makes the complement also a reject for searching for a head/tail link, as any newly found node with such would implicitly form a branch.

### 3. Solutions

Because solutions are by their nature cyclic structures, they cannot be sorted in this form for searching. Thus instead, they are attached to a growing linear array for storing candidates not yet closed before or after the cycle target, and a second structure stores a linked list that keeps separate head/tail pairs in a linked list for fast matching against newly generated members of the hash chain.

The array is searched exhaustively with new candidate vectors, as each as yet incomplete solution chain stores the progress of discovery of a cycle. If a branch is found the candidate is rejected. If it closes before the required cycle length, it is rejected. If it grows longer than the required cycle length, it is rejected.

### 4. Rejects

Rejects are even less sortable. But they don't need to be searched except for the individual head and tails that are all at least in pairs.

So for this structure, one node keeps them all together in a linear list (using one parent and one child link) pointing to their node, it does not have to be sorted, they are just appended in the order found.

A second dual linked list keeps a sorted list of every head/tail coordinate found to be part of a cycle, which is fast to search, and any candidate vector containing these coordinates means a branch and thus will then have its both members added to the reject linked list.
