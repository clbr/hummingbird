// Tests for the Chain
package main

import (
	"log"
	"time"

	"gitlab.com/clbr/hummingbird/pkg/chain"
)

const (
	maxIndices = 1<<32 - 1
)

// Solver - Searches for a collision pattern in a hash chain
func main() {
	startTime := time.Now()
	// create new hashchain
	chain := chain.New(maxIndices)
	// create head/tail search tree
	// search := chain.NewSearch()
	// create first vector
	chain.Grow()
	solFound := false
	for !solFound && chain.Last < maxIndices {
		// add a new vector to HashChain
		chain.Grow()
	}
	elapsedTime := time.Since(startTime)
	log.Println("Time to generate", len(chain.Chain), "hashes", elapsedTime, "Memory used for HashChain", (len(chain.Chain)*4)/1024/1024/1024, "Gb")
}
