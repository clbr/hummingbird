package main

import (
	"log"
	"time"

	"gitlab.com/clbr/hummingbird/pkg/chain"
	"gitlab.com/clbr/hummingbird/pkg/hb"
)

const (
	maxIndices = 1<<32 - 1
)

func main() {
	log.Println("Generating complete 16gb hashchain")
	startTime := time.Now()
	hc := chain.New(maxIndices)
	for i := 0; i < maxIndices; i++ {
		hc.Grow()
	}
	elapsedTime := time.Since(startTime)
	log.Println("Time to generate", hc.Last, "hashes", elapsedTime, "Memory used for HashChain", (uint64(hc.Last)+1)*4/1024/1024/1024, "Gb")
	log.Println("running collision search")
	var i, j, hh, ht, tt, th, total uint32
	for i = 1; i < hc.Last; i++ {
		startTime = time.Now()
		head := hb.Head(hc.Chain[i-1])
		tail := hb.Tail(hc.Chain[i-1])
		for j = 1; j < hc.Last; j++ {
			// don't test item itself
			if i != j {
				testHead := hb.Head(hc.Chain[j-1])
				testTail := hb.Tail(hc.Chain[j-1])
				if head == testHead {
					// log.Println("Found head/head collision at", j, "from head of", i, ":", head, tail, "with values", testHead, testTail)
					hh++
				}
				if head == testTail {
					// log.Println("Found head/tail collision at", j, "from head of", i, ":", head, tail, "with values", testHead, testTail)
					ht++
				}
				if tail == testTail {
					// log.Println("Found tail/tail collision at", j, "from head of", i, ":", head, tail, "with values", testHead, testTail)
					tt++
				}
				if tail == testHead {
					// log.Println("Found tail/head collision at", j, "from head of", i, ":", head, tail, "with values", testHead, testTail)
					th++
				}
				if head == testHead && tail == testTail {
					total++
					log.Println("total collision found at", j, "from", i)
				}
			}
		}
		elapsedTime = time.Since(startTime)
		log.Println("found collisions from", i, head, tail, ":", hh, "h/h,", ht, "h/t,", tt, "t/t,", th, "t/h and", total, "total collisions in", elapsedTime)
		hh, ht, tt, th, total = 0, 0, 0, 0, 0
	}

}
