package main

import (
	"log"

	"github.com/spaolacci/murmur3"
	"gitlab.com/clbr/hummingbird/pkg/hb"
)

// Search the murmur3 hash chain field for repetition cycle variance
func main() {
	var maxIndices uint
	found := 0
	maxtries := 1 << 10
	for index := uint8(10); index < 32; index++ {
		maxIndices = 1 << index
		for k := 0; k < maxtries; k++ {
			for j := 0; j < int(maxIndices); j++ {
				nonce := hb.GetRandUint32()
				first := murmur3.Sum32(hb.Uint32ToBytes(nonce))
				subsequent := first
				for i := 1; i < int(maxIndices); i++ {
					subsequent = murmur3.Sum32(hb.Uint32ToBytes(subsequent))
					if subsequent == first {
						i = int(maxIndices)
						found++
					}
				}
				if first == subsequent {
					j = int(maxIndices)
				}
			}
			if k == 0 {
				log.Println("attempt", k, "at 2 ^", index, "chain length")
			}
		}
		log.Println("Solutions found:", found, "out of", maxtries, "attempts")
		found = 0
	}
}
