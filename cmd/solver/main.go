package main

import (
	"fmt"

	"gitlab.com/clbr/hummingbird/pkg/root"
)

const (
	maxIndices = 1<<32 - 1
)

func main() {
	searchRoot := root.New(maxIndices)
	searchRoot.Chain.Grow()
	fmt.Println(searchRoot)
}
